const pulumi = require("@pulumi/pulumi");

pulumi.runtime.setMocks({
    newResource: function(args) {
        return {
            id: args.inputs.name + "_id",
            state: args.inputs,
        };
    },
    call: function(args) {
        return args.inputs;
    },
});