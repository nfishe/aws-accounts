module.exports = {
    roots: [
        "<rootDir>"
    ],
    setupFilesAfterEnv: ["./setup.js"],
    testMatch: ['**/*.test.ts', '**/*.test.tsx'],
    moduleFileExtensions: [
        "js",
        "ts",
        "tsx",
        "json"
    ],
    preset: 'ts-jest'
};