import * as pulumi from "@pulumi/pulumi";
import { gitlabOidc } from "./index";

describe("infrastructure", () => {
  describe("#oidc", () => {
    it("should have gitlab url", () => {
      pulumi.output(gitlabOidc.url).apply((v) => {
        expect(v).toBe("https://gitlab.com");
      });
    });

    it("should have thumbprints", () => {
        pulumi.output(gitlabOidc.thumbprintLists).apply((v) => {
            expect(v).toHaveLength(1);
          });        
    });
  });
});
