import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";
import * as tls from "@pulumi/tls";

const org = new aws.organizations.Organization("org");
new aws.organizations.Account("ndfishe", {
  email: "ndfishe@gmail.com",
  name: "ndfishe",
  parentId: org.roots[0].id,
});

const gitlabUrl = "https://gitlab.com";
const cert = tls.getCertificateOutput({
  url: gitlabUrl,
});

export const gitlabOidc = new aws.iam.OpenIdConnectProvider("gitlab", {
  url: gitlabUrl,
  clientIdLists: [gitlabUrl],
  thumbprintLists: [
    pulumi.interpolate`${cert.certificates[0].sha1Fingerprint}`,
  ],
});

interface GitlabCIArgs {
  readonly roleArn: pulumi.Input<string>;
  readonly match: pulumi.Input<string>;
}

class GitlabCI extends pulumi.ComponentResource {
  public readonly role: aws.iam.Role;
  public readonly arn: pulumi.Output<string>;

  constructor(
    name: string,
    args: GitlabCIArgs,
    opts?: pulumi.ComponentResourceOptions
  ) {
    super("ndfishe:gitlab-ci", name, {}, opts);

    const policy = aws.iam.getPolicyDocumentOutput(
      {
        statements: [
          {
            actions: ["sts:AssumeRoleWithWebIdentity"],
            principals: [
              {
                type: "Federated",
                identifiers: [args.roleArn],
              },
            ],
            effect: "Allow",
            conditions: [
              {
                test: "StringLike",
                variable: args.match,
                values: ["project_path:nfishe/*:ref_type:branch:ref:*"],
              },
            ],
          },
        ],
      },
      { parent: this }
    );

    const adminPolicy = pulumi.output(
      aws.iam.getPolicy(
        {
          arn: "arn:aws:iam::aws:policy/AdministratorAccess",
        },
        { parent: this }
      )
    );

    this.role = new aws.iam.Role(
      name,
      {
        namePrefix: "GitlabCI",
        assumeRolePolicy: policy.json,
        managedPolicyArns: [adminPolicy.arn],
        path: "/",
      },
      { parent: this }
    );

    this.arn = this.role.arn;

    this.registerOutputs();
  }
}

let match = gitlabOidc.url.apply((url) => `${url}:sub`);
const gitlabCi = new GitlabCI("gitlab-ci", {
  roleArn: pulumi.interpolate`${gitlabOidc.arn}`,
  match,
});

export const roleArn = gitlabCi.arn;
